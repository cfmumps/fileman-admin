<cfcontent type="application/json">
<cfsilent>
<cfset db = createObject("component", "lib.cfmumps.mumps")>
<cfset db.open()>

<cfset util = createObject("component", "lib.cfmumps.util")>

<cfset fileList = []>
<cfset lastResult = false>
<cfset nextSubscript = "">

<cfloop condition="lastResult EQ false">
	<cfset os = {}>
	<cfset order = db.order("DIC", [nextSubscript])>
	<cfset lastResult = order.lastResult>
	<cfset nextSubscript = order.value>


	<cfif nextSubscript NEQ "" AND nextSubscript NEQ 0>
		<cfset fileName = util.getPiece(db.get("DIC", [nextSubscript, 0]), "^", 1)>
		<cfset os.ddNumber = order.value>
		<cfset os.fileName = fileName>
		<cfset fileList.append(os)>
	</cfif>
</cfloop>
<cfset db.close()>
</cfsilent>
<cfoutput>#serializeJSON(fileList)#</cfoutput>

