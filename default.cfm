<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="cfmumps FileMan Administrator">
	<meta name="author" content="Coherent Logic Development LLC">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.css">
	<link rel="stylesheet" href="typeaheadjs.css">
	<link rel="stylesheet" href="style.css">
	<title>FileMan Viewer</title>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="navbar navbar-inverse navbar-static-top" role="navigation">

    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" rel="home" href="#" title="Home">FileMan Viewer</a>
    </div>

    <div class="collapse navbar-collapse navbar-ex1-collapse">


        <div class="col-sm-3 col-md-3 pull-right">
        <form class="navbar-form" role="search">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Enter filename to open" name="fileChooser" id="fileChooser">
        </div>
        </form>
        </div>

    </div>
</div>

    <div class="container" id="container">
	    <h2 id="fileName">No file selected</h2>
	    <ul class="nav nav-tabs" role="tablist">
		    <li class="active"><a href="#overview" role="tab" data-toggle="tab">Overview</a></li>
		    <li><a href="#fields" role="tab" data-toggle="tab">Fields <span class="badge" id="fieldCount"></span></a></li>
		    <li><a href="#indexes" role="tab" data-toggle="tab">Indexes <span class="badge" id="indexCount"></span></a></li>
		    <li><a href="#security" role="tab" data-toggle="tab">Security</a></li>
		    <li><a href="#fileData" role="tab" data-toggle="tab">Entries <span class="badge" id="entryCount"></span></a></li>
	    </ul>

	    <div class="tab-content">
		    <div class="tab-pane active" id="overview"><p>Please select a file</p></div>
		    <div class="tab-pane" id="fields"></div>
		    <div class="tab-pane" id="indexes"></div>
		    <div class="tab-pane" id="security"></div>
		    <div class="tab-pane" id="fileData"></div>
	    </div>
    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="js/app.js"></script>
    <script src="js/typeahead.js"></script>
    <script src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
    <script src="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.js"></script>
    <script language="javascript">
	    $(document).ready(function () {
	            return(onReady());
	    });
    </script>

<div class="modal fade" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
	<div class="modal-content">
        <div class="modal-body">
	<div class="modal-header">
		<h2>Loading File Definitions...</h2>
		<p>This can take up to 45 seconds. Please be patient.</p>

		
	</div>
<div class="progress">
  <div class="progress-bar progress-bar-striped active"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
    <span class="sr-only">Loading Files</span>
  </div>
</div>
        </div>
	</div>
	</div>
</div>


</body>
</html>

