<div style="padding:20px;">
<div class="input-group" style="margin-bottom:20px;">
            <cfoutput><input type="text" class="form-control" placeholder="Search Fields" name="searchFields" id="#attributes.baseId#_searchFields"></cfoutput>
            <div class="input-group-btn">
                <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
            </div>
</div>
<cfloop array="#session.file.fieldNumbers#" index="field">
	<cfswitch expression="#session.file.fields[field].dataType#">
		<cfcase value="string">
			<cfset typeCode="FT">
		</cfcase>
		<cfcase value="enum">
			<cfset typeCode="SC">
		</cfcase>
		<cfcase value="numeric">
			<cfset typeCode="N">
		</cfcase>
		<cfcase value="pointer">
			<cfset typeCode="P">
		</cfcase>
		<cfcase value="date">
			<cfset typeCode="D">
		</cfcase>
		<cfcase value="text">
			<cfset typeCode="WP">
		</cfcase>
		<cfcase value="varpointer">
			<cfset typeCode="VP">
		</cfcase>
		<cfcase value="computed">
			<cfset typeCode="C">
		</cfcase>
		<cfcase value="mumps">
			<cfset typeCode="M">
		</cfcase>
	</cfswitch>
	<cfoutput>
		<div class="fileList" id="#attributes.baseId#_field_#field#">
			<div style="display:inline-block; min-width: 40px;">
				<span class="badge">#typeCode#</span>
			</div>
			<div style="display:inline-block; min-width: 40px;">
				#field#
			</div>
			<cfif isDefined("attributes.onselect")>
				<input type="checkbox" 
				       id="#attributes.baseId#_fieldSelect_#field#"
				       onclick="#attributes.onSelect#('#field#', '#attributes.baseId#');">
			</cfif>
			#session.file.fields[field].fieldLabel#
		</div>
	</cfoutput>
</cfloop>
</div>
