var fileNames = null;
var fileData = null;
var loaderHTML = '<div class="loadSpinner"><img src="images/loading.gif"><h2>Loading...</h2></div>';
var selectedFields = [];

function onReady()
{

    $("#pleaseWaitDialog").modal('show');

    $.get('data/files/list.cfm', function(data) {
	if(typeof data == "object") {
	    fileNames = data;
	}
	else {
	    fileNames = JSON.parse(data);
	}
	
	$("#pleaseWaitDialog").modal('hide');

	var files = new Bloodhound({
	    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('fileName'),
	    queryTokenizer: Bloodhound.tokenizers.whitespace,
	    local: fileNames
	});

	files.initialize();

	$("#fileChooser").typeahead(null, {
	    name: 'files',
	    displayKey: 'fileName',
	    source: files.ttAdapter()
	}).on('typeahead:selected', function(event, data) {
	    openFile(data.ddNumber, data.fileName);
	});
	
    });

    return(true);
}

function openFile(ddNumber, fileName)
{
    $("#fileName").html(ddNumber + ': ' + fileName);
    $(".tab-pane").html(loaderHTML);

    $("#entryCount").html('');
    $("#fieldCount").html('');

    var sessionSetupUrl = 'initSession.cfm?ddNumber=' + escape(ddNumber);

    $.get(sessionSetupUrl, function(data) {

	fileData = JSON.parse(data);

	$("#entryCount").html(fileData.entryCount);
	$("#fieldCount").html(fileData.fieldNumbers.length);

	var overviewUrl = 'overview.cfm';
    
	$("#overview").load(overviewUrl, function() {
	    $("#overview_searchFields").on('keyup', function() {
		searchFields('overview');
	    });
	});
	
	var fieldsUrl = 'fields.cfm';
	
	$("#fields").load(fieldsUrl, function() {
	    
	});
	
	var indexesUrl = 'indexes.cfm';
	
	$("#indexes").load(indexesUrl, function() {
	    
	});
	
	var securityUrl = 'security.cfm';
	
	$("#security").load(securityUrl, function() {
	    
	});

	var dataUrl = 'data.cfm';
	
	$("#fileData").load(dataUrl, function() {
	    $("#data_searchFields").on('keyup', function() {
		searchFields('data');
	    });
	    
	});
    });
}  

function searchFields(baseId) 
{
    var searchBoxSelector = "#" + baseId + "_searchFields";
    var searchValue = $(searchBoxSelector).val();


    for(i = 1; i < fileData.fieldNumbers.length; i++) {
	var fieldSelector = "#" + baseId + "_field_" + escapeSelector(fileData.fieldNumbers[i]);
	if(fileData.fields[fileData.fieldNumbers[i]].fieldLabel.indexOf(searchValue) > -1) {
	    $(fieldSelector).show();
	}
	else {
	    $(fieldSelector).hide();
	}
    }
}

function escapeSelector(s){
    return s.replace( /(:|\.|\[|\])/g, "\\$1" );
}

function selectField(fieldNumber, baseId)
{
    var selector = '#' + baseId + '_fieldSelect_' + escapeSelector(fieldNumber);
    var checked = $(selector).is(':checked');

    if(checked) {
	selectedFields.push(fieldNumber);
    }
    else {
	for(var i = 0; i < selectedFields.length; i++) {
	    if(selectedFields[i] === fieldNumber) {
		selectedFields.splice(i, 1);
	    }
	}
    }

    return(true);
}
