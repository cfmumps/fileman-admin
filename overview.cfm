<div class="row">
<div class="col-md-4">
	<cfmodule template="fieldList.cfm" baseId="overview">
</div>
<div class="col-md-8">
<div style="padding:20px;">
<cfoutput>
<form role="form">
	<div class="form-group">
		<label for="ovwFileName">File name</label>
		<input type="text" class="form-control" id="ovwFileName" value="#session.file.fileName#">
	</div>
	<div class="form-group">
		<label for="ovwFileNumber">File DD number</label>
		<input type="text" class="form-control" id="ovwFileNumber" value="#session.file.ddNumber#">
	</div>
	<div class="form-group">
		<label for="ovwEntryCount">Entry count</label>
		<input type="text" class="form-control" id="ovwEntryCount" value="#session.file.entryCount#">
	</div>
	<div class="form-group">
		<label for="ovwMostRecentIEN">Most recently-assigned IEN</label>
		<input type="text" class="form-control" id="ovwMostRecentIEN" value="#session.file.mostRecentIEN#">
	</div>
	<div class="form-group">
		<label for="ovwGlobalRoot">Global root</label>
		<input type="text" class="form-control" id="ovwGlobalRoot" value="#session.file.globalRoot#">
	</div>
</form>
</cfoutput>
</div>
</div>


